#include <GL/glu.h>
#include <GL/glut.h>
#include <QGLWidget>
#include "SolidCubeWidget.h"


// constructor
SolidCubeWidget::SolidCubeWidget(QWidget *parent)
	: QGLWidget(parent)
	{ // constructor
       

	} // constructor

// called when OpenGL context is set up
void SolidCubeWidget::initializeGL()
	{ // initializeGL()
	// set the widget background colour
	glClearColor(0.3, 0.3, 0.3, 0.0);
	} // initializeGL()


// called every time the widget is resized
void SolidCubeWidget::resizeGL(int w, int h)
	{ // resizeGL()
	// set the viewport to the entire widget
	glViewport(0, 0, w, h);

        
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
        glFrustum(-1.0,1.0, -1.0, 1.0, 1.5, 20.0);
        glMatrixMode(GL_MODELVIEW);

	} // resizeGL()


	
// called every time the widget needs painting
void SolidCubeWidget::paintGL()
	{ // paintGL()
	// clear the widget
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

        glColor3f(1.0,0.0,0.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(0.,2.,5,0.,0.,0.,0.,1.,0.);
	glScalef(1.0,2.0,1.0);
	glutWireCube(1.0);

	glTranslatef(1.,0.,1.);
        glColor3f(0.0,1.0,0.0);
	glutWireCube(1.0);

	glTranslatef(-2.,0.,-15.);
        glColor3f(0.0,0.0,1.0);
	glutWireCube(1.0);


	// flush to screen
	glFlush();	
	} // paintGL()
