#include <GL/glu.h>
#include <QGLWidget>
#include <QSlider>
#include <cmath>
#include <iostream>
#include "HierarchyWidget.h"

double HEAD_HEIGHT    = 1.5;
double ARM_LENGTH     = 3.0;
double SHOULDER_WIDTH = 3.0;

// Setting up material properties
typedef struct materialStruct {
  GLfloat ambient[4];
  GLfloat diffuse[4];
  GLfloat specular[4];
  GLfloat shininess;
} materialStruct;


static materialStruct brassMaterials = {
  { 0.33, 0.22, 0.03, 1.0},
  { 0.78, 0.57, 0.11, 1.0},
  { 0.99, 0.91, 0.81, 1.0},
  27.8
};

static materialStruct whiteShinyMaterials = {
  { 1.0, 1.0, 1.0, 1.0},
  { 1.0, 1.0, 1.0, 1.0},
  { 1.0, 1.0, 1.0, 1.0},
  100.0
};

// constructor
HierarchyWidget::HierarchyWidget(QWidget *parent)
  : QGLWidget(parent),
    angle(0),
    armangle(0)
	{ // constructor
	  phead      = gluNewQuadric();
	  pshoulders = gluNewQuadric();
	  ptorso     = gluNewQuadric();
	  pupperleft = gluNewQuadric();
	  plowerleft = gluNewQuadric();

	} // constructor

// called when OpenGL context is set up
void HierarchyWidget::initializeGL()
	{ // initializeGL()
	// set the widget background colour
	glClearColor(0.3, 0.3, 0.3, 0.0);

 	} // initializeGL()

HierarchyWidget::~HierarchyWidget()
{
  gluDeleteQuadric(phead);
  gluDeleteQuadric(pshoulders);
  gluDeleteQuadric(ptorso);
  gluDeleteQuadric(pupperleft);
  gluDeleteQuadric(plowerleft);
}

void HierarchyWidget::updateAngle(){
  angle += 1.;
  this->repaint();
}

void HierarchyWidget::updateArmAngle(int i){
  // find out about the object that sent
  armangle = i;
  this->repaint();
}

// called every time the widget is resized
void HierarchyWidget::resizeGL(int w, int h)
	{ // resizeGL()
	// set the viewport to the entire widget
	glViewport(0, 0, w, h);
	GLfloat light_pos[] = {1., 0.5, 1., 0.};

	glEnable(GL_LIGHTING); // enable lighting in general
        glEnable(GL_LIGHT0);   // each light source must also be enabled

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	materialStruct* p_front = &brassMaterials;

       	glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
	glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);

	glLightfv(GL_LIGHT0, GL_POSITION, light_pos);
       	glLightf (GL_LIGHT0, GL_SPOT_CUTOFF,150.);


	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-7.0, 7.0, -7.0, 7.0, -7.0, 7.0);

	} // resizeGL()

void HierarchyWidget::head(){
  gluSphere(phead,1.0,20,20);
}

void HierarchyWidget::torso(){
  gluCylinder(ptorso,1.0,1.0,4,5,5);
}


void HierarchyWidget::shoulders(){
  gluCylinder(pshoulders,0.5,0.5,3,4,4);
}


void HierarchyWidget::upper_arm_left(){
  gluCylinder(pupperleft,0.5,0.5,3,4,4);
}

void HierarchyWidget::lower_arm_left(){
  gluCylinder(plowerleft,0.3,0.3,3,8,8);
}


// called every time the widget needs painting
void HierarchyWidget::paintGL()
	{ // paintGL()
	// clear the widget
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

      	glShadeModel(GL_SMOOTH);

	// You must set the matrix mode to model view directly before enabling the depth test
      	glMatrixMode(GL_MODELVIEW);
       	glEnable(GL_DEPTH_TEST); // comment out depth test to observe the result


	// global rotation angle
      //  glRotatef((double)angle,0.,1.,0.);

	materialStruct* p_front = &whiteShinyMaterials;

       	glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
	glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);

	// creating the torso
	glPushMatrix(); // save the world
      	glRotatef(90.,1.,0.,0.);
	this->torso();
	glPopMatrix(); // restore the world


	p_front = &brassMaterials;

       	glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
	glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);

	// shoulders
	glPushMatrix();
      	glTranslatef(0.,0.,-SHOULDER_WIDTH/2);
	this->shoulders();
	glPopMatrix();

	// placing the head
	glPushMatrix(); // save the global rotation state
	glTranslatef(0., HEAD_HEIGHT, 0.);
	this->head();
	glPopMatrix();  // restore the original matrix


	// upper arms
	glPushMatrix();
	glRotatef((double)armangle,0.,0.,1.);
	glTranslatef(0.,0., SHOULDER_WIDTH/2.);
       	glRotatef(90,1.,0.,0.);
	this->upper_arm_left();
	// Now we stay in the frame of the upper arm!!!!

	glTranslatef(0.,0.,ARM_LENGTH); // move to the end of the arm;
                                        // this will be the new origin of the frame of the lower arm
	glRotatef((double)armangle*2,0.,1.,0.);
	this->lower_arm_left();
	glPopMatrix();


	glLoadIdentity();
       	gluLookAt(1.,1.,1., 0.0,0.0,0.0, 0.0,1.0,0.0);

	// flush to screen
	glFlush();

	} // paintGL()
