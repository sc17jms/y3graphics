#ifndef __GL_POLYGON_WIDGET_H__
#define __GL_POLYGON_WIDGET_H__ 1

#include <QGLWidget>
#include <GL/glut.h>

class HierarchyWidget: public QGLWidget
	{ // 

	Q_OBJECT

	public:

	HierarchyWidget(QWidget *parent);
	~HierarchyWidget();

	public slots:
	void updateAngle();
	
	public slots:
        void updateArmAngle(int i);
	

	protected:
	// called when OpenGL context is set up
	void initializeGL();
	// called every time the widget is resized
	void resizeGL(int w, int h);
	// called every time the widget needs painting
	void paintGL();

	private:

	void head();
	void torso();
	void shoulders();
	void upper_arm_left();
	void lower_arm_left();

	GLUquadricObj* phead;
	GLUquadricObj* ptorso;
	GLUquadricObj* pshoulders;
	GLUquadricObj* pupperleft;
	GLUquadricObj* plowerleft;
	

	int angle;
	int armangle;

	}; // class GLPolygonWidget
	
#endif
