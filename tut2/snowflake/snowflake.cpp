#include <QtGui>
#include <QApplication>
#include <QMainWindow>
#include <QMessageBox>
#include <QPainter>
#include <QDesktopWidget>
#include <iostream>
//used for timer sleep
#include <unistd.h>


class MyWidget : public QWidget {
public:

  // mouse tracking must be enabled
  void EnableMouse();
  void EnableKeyboard();
protected:

  virtual void paintEvent(QPaintEvent*);
  virtual void mouseMoveEvent(QMouseEvent*);
  virtual void keyPressEvent(QKeyEvent*);

private:

};

void MyWidget::EnableMouse()
{
  // you must set mouse tracking, it's off by default
  this->setMouseTracking(true);
}

void MyWidget::EnableKeyboard()
{
  // you must set mouse tracking, it's off by default
  this->setFocusPolicy(Qt::StrongFocus);
}


void MyWidget::keyPressEvent(QKeyEvent *event)
{
  int key=event->key();
  QString keyString;
  keyString=QString(key);
  QMessageBox::information(this,"snowflake",keyString);
}

void MyWidget::mouseMoveEvent(QMouseEvent *_event)
{
  if (this->rect().contains(_event->pos())) {
    // Mouse over Widget
    QMessageBox::information(this,"snowflake","you moved the mouse");
    std::cout<<"mouse\n";
    usleep(200);
  }
  else {
    // Mouse out of Widget
  }
}

void MyWidget::paintEvent( QPaintEvent * )
{
  QPainter p( this );
  p.setPen( Qt::darkGray );
  p.drawRect( 1,2, 5,4 );
  p.setPen( Qt::lightGray );
  p.drawLine( 9,2, 7,7 );
}


// I don't like the default way the main window pops up, I want it bigger
class MyMainWindow : public QMainWindow {

public:

  MyMainWindow();

};

MyMainWindow::MyMainWindow(){
  // my window asks how big the desktop is; this is a widget for QT
  resize(QDesktopWidget().availableGeometry(this).size() * 0.7);
  // I've set my window to 70 % of the desktop
}

int main(int argc, char **argv) {
  QApplication app(argc, argv);
  MyMainWindow window;

  MyWidget w;
  w.setFocus();
  w.EnableKeyboard();
  w.EnableMouse(); // don't forget this
  window.setCentralWidget(&w);
  window.show();

  return app.exec();
}
