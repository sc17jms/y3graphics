#ifndef __GL_POLYGON_WIDGET_H__
#define __GL_POLYGON_WIDGET_H__ 1

#include <QGLWidget>
#include <QSlider>
#include <QTimer>
#include <QMainWindow>
#include <QObject>


class SolidCubeWidget: public QGLWidget
	{ //
	Q_OBJECT

	public:
	SolidCubeWidget(QWidget *parent);

	void MyTimer();
	QTimer *timer;

	public slots:
	void update_timer();


	protected:
	// called when OpenGL context is set up
	void initializeGL();
	// called every time the widget is resized
	void resizeGL(int w, int h);
	// called every time the widget needs painting
	void paintGL();

	void rotate();

	private:
	void teapot();
	void cube();
	void polygon(int, int, int, int);

	double rotation;

	}; // class GLPolygonWidget

#endif
