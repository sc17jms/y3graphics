#include <QtGui>
#include <QApplication>
#include <QMainWindow>
#include <QMessageBox>
#include <QPainter>
#include <QLabel>
#include <QDesktopWidget>
#include <iostream>

class MyWidget : public QWidget {
public:
  MyWidget() : QWidget() { setFocusPolicy(Qt::StrongFocus); }
protected:
 
 void keyPressEvent(QKeyEvent *ke) {
   QMessageBox::information(this,"snowflake","That hurts! You ******* sadist!!!!!!");

   //    qDebug() << ke->key();
  }

  void paintEvent( QPaintEvent * )
  {
    // you might be tempted to make QPainter a class variable,
    // but the documentation warns explicitly against that

    QPainter p( this );


    // --------------Uncomment the next line--------------
    //  p.setWindow(QRect(-50, -50, 100, 100)); // this now creates a logical space  from (-50, -50) to (50, 50)
    // in these coordinates from bottom-left to top-right
    // if the aspect ratio changes, the figure becomes deformed
    // unless a viewport is defined

    // ---------------Uncomment the next two lines----------------- 
    // int side = qMin(width(), height());  
    // p.setViewport(0, 0, side, side);

    p.setPen( Qt::darkGray );
    p.drawRect( -25,-25, 50, 50 );

  }

};
 

