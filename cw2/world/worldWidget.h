#ifndef __GL_POLYGON_WIDGET_H__
#define __GL_POLYGON_WIDGET_H__ 1

#include <QGLWidget>
#include <QSlider>
#include <QTimer>
#include <QMainWindow>
#include <QObject>
#include <GL/glut.h>
#include "Image.h"


class worldWidget: public QGLWidget
	{ //
	Q_OBJECT

	public:
	worldWidget(QWidget *parent);

	void MyTimer();
	QTimer *timer;
	bool ROTATE;
	bool DAY;
	bool ANIMATE;
	bool FLAP;
	double zoom;
	double flapAngle;

	public slots:
	void update_timer();
	void updateZoom(int);
	void toggleDayNight();
	void toggleRotation();
	void animate();


	protected:
	// called when OpenGL context is set up
	void initializeGL();
	// called every time the widget is resized
	void resizeGL(int w, int h);
	// called every time the widget needs painting
	void paintGL();

	void rotate();




	//note to self do not allow backface culling or the globe will be invisible when inside.

	private:
	void base();
	void octagon();
	void globe();
	void faceglobe();
	void house(); //will have emission windows so it look like ther are lights on in the house at night.
	void tree();
	void sky(); //not sure whether to make two objects sun and moon or change material properties of one object.
	void map();
	void roof();
	void bird();
	void worldmap();

	void internal();

	GLUquadricObj* ptorso;
	GLUquadricObj* pshoulders;
	GLUquadricObj* pupperl;
	GLUquadricObj* plowerl;
	GLUquadricObj* pupperr;
	GLUquadricObj* plowerr;
	GLUquadricObj* plegl;
	GLUquadricObj* plegr;
	GLUquadricObj* phands;
	GLUquadricObj* paxe;

	void dude();

	//Image _image;
	Image _image1;
	Image _image2;
	// Image moi_image;
	// Image globe_image;
	int angle;

	void cube();
	void trunk();
	void cone();
	void polygon(int, int, int, int);



	double armangle;
	double birdrotation;
	double rotation;
	double skyrotation;


	}; // class GLPolygonWidget

#endif
