#include <GL/glu.h>
#include <QGLWidget>
#include <QSlider>
#include <QTimer>

#include "worldWindow.h"
#include <cmath>
#include <iostream>
#include <QCoreApplication>
#include "Image.h"


// Setting up material properties
typedef struct materialStruct {
  GLfloat ambient[4];
  GLfloat diffuse[4];
  GLfloat specular[4];
  GLfloat shininess;
} materialStruct;


//materials from http://www.it.hiof.no/~borres/j3d/explain/light/p-materials.html
//mostly
//a little bit of tinkering

static materialStruct brassMaterials = {
  { 0.33, 0.22, 0.03, 1.},
  { 0.78, 0.57, 0.11, 1.},
  { 0.1, 0.1, 0.1, 1.},
  0.05
};

static materialStruct mattBlack = {
  { 0,0,0, 1.},
  { 0,0,0, 1.},
  { 0,0,0, 1.},
  0.5

  // { 0.033, 0.022, 0.003, 1.},
  // { 0.078, 0.057, 0.011, 1.},
  // { 0.01, 0.01, 0.01, 1.},
  // 0.0005
};

static materialStruct brownMaterials = {
  { 0.05, 0.01, 0.06475, 1.0},
  { 0.07, 0.02368, 0.01036, 1.0},
  { 0.01, 0.011, 0.021, 1. },
  0.05
};

static materialStruct greenMaterials = {
  { 0.01,0.1,0.01,1.0},
  { 0.01,0.1,0.01,1.0},
  {0.01,0.1,0.01,1.0},
  0.01
};

static materialStruct beigeMaterials = { //need to do
  { 0.25, 0.148, 0.06475, 1.0},
  { 0.5, 0.3368, 0.2036, 1.0},
  { 0.774597, 0.458561, 0.200621, 1.0 },
  .01
};

static materialStruct redMaterials = {
  { 0.1745, 0.01175, 0.01175, 1.},
  { 0.61424, 0.04136, 0.04136, 1.},
  { 0.1, 0.02, 0.02, 1. },
  0.05
};

static materialStruct blueMaterials = {
  { 0.01175,0.01175,0.1745 ,1.},
  { 0.04136,0.04136,0.61424,1.},
  {0.02,0.02,0.1,1.},
  .05
};

static materialStruct glassMaterials = {
  { 0.1,0.1,0.06 ,1},
  { 0.01,0.50980392,0.50980392,0.7},
  {0.50196078,0.50196078,0.50196078,1},
  5.
};

static materialStruct sunMaterials = {
  { 0.2, 0.1, 0.01175, 1.},
  { 0.2, 0.1, 0.01175, 1.},
  { 0.2, 0.1, 0.01175, 1.},
  0.05
};

static materialStruct whiteShinyMaterials = {
  { 1.0, 1.0, 1.0, 1.0},
  { 1.0, 1.0, 1.0, 1.0},
  { 1.0, 1.0, 1.0, 1.0},
  100.0
};


// constructor
worldWidget::worldWidget(QWidget *parent)
	: QGLWidget(parent),
    angle(0),
    armangle(0),
    _image1("./textures/Moi.ppm"),
    _image2("./textures/earth.ppm")


	{ // constructor
    pshoulders= gluNewQuadric();
    ptorso    = gluNewQuadric();
    pupperl   = gluNewQuadric();
    plowerl   = gluNewQuadric();
    pupperr   = gluNewQuadric();
    plowerr   = gluNewQuadric();
    plegl     = gluNewQuadric();
    plegr     = gluNewQuadric();
    phands    = gluNewQuadric();
    paxe      = gluNewQuadric();
	} // constructor

// called when OpenGL context is set up
void worldWidget::initializeGL()
	{ // initializeGL()
	// set the widget background colour
	glClearColor(0.3, 0.3, 0.3, 0.0);
  MyTimer();
	glEnable(GL_TEXTURE_2D);
  zoom=0.25;
  armangle=-30;
  ROTATE=true;
  DAY=true;
  FLAP=true;
  birdrotation=0;
  flapAngle=0;

 	} // initializeGL()

// called every time the widget is resized
void worldWidget::resizeGL(int w, int h)
	{ // resizeGL()
	// set the viewport to the entire widget
	glViewport(0, 0, w, h);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();


	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	//glOrtho(-7.0, 7.0, -7.0, 7.0, -7.0, 7.0);
  gluPerspective(60,1,1,6); //real trippy
  //glFrustum(-10.0, 10.0, -10.0, 10.0, -10.0, 10.0);
	} // resizeGL()






//////////////////////////
///
// Models defined below
///
/////////////////////////




const int NR_PHI   = 40;
const int NR_THETA = 40;

void worldWidget::globe(){

  for(int longitude = 0; longitude < NR_PHI; longitude++)
    for(int latitude = 0; latitude < NR_THETA; latitude++){
      float d_phi   = 2*M_PI/NR_PHI;
      float d_theta = M_PI/NR_THETA;
      glBegin(GL_TRIANGLES);
      double x, y, z;

      x = cos(longitude*d_phi)*sin(latitude*d_theta);
      y = sin(longitude*d_phi)*sin(latitude*d_theta);
      z = cos(latitude*d_theta);
      glNormal3f(x, y, z);

      //glTexCoord2f(static_cast<float>(longitude)/NR_PHI, static_cast<float>(latitude)/NR_THETA);

      glVertex3f(x, y, z);
      x = cos((longitude+1)*d_phi)*sin(latitude*d_theta);
      y = sin((longitude+1)*d_phi)*sin(latitude*d_theta);
      z = cos(latitude*d_theta);
      glNormal3f(x, y, z);

      //glTexCoord2f(d_phi,0);
      //glTexCoord2f(static_cast<float>(longitude+1)/NR_PHI, static_cast<float>(latitude)/NR_THETA);

      glVertex3f(x, y, z);
      x = cos((longitude+1)*d_phi)*sin((latitude+1)*d_theta);
      y = sin((longitude+1)*d_phi)*sin((latitude+1)*d_theta);
      z = cos((latitude+1)*d_theta);
      glNormal3f(x, y, z);


      //glTexCoord2f(static_cast<float>(longitude+1)/NR_PHI, static_cast<float>(latitude+1)/NR_THETA);


      glVertex3f(x, y, z);

      x = cos(longitude*d_phi)*sin(latitude*d_theta);
      y = sin(longitude*d_phi)*sin(latitude*d_theta);
      z = cos(latitude*d_theta);
      glNormal3f(x, y, z);


      //glTexCoord2f(static_cast<float>(longitude)/NR_PHI, static_cast<float>(latitude)/NR_THETA);


      glVertex3f(x, y, z);
      x = cos((longitude+1)*d_phi)*sin((latitude+1)*d_theta);
      y = sin((longitude+1)*d_phi)*sin((latitude+1)*d_theta);
      z = cos((latitude+1)*d_theta);
      glNormal3f(x, y, z);

      //glTexCoord2f(static_cast<float>(longitude+1)/NR_PHI, static_cast<float>(latitude+1)/NR_THETA);


      glVertex3f(x, y, z);
      x = cos((longitude)*d_phi)*sin((latitude+1)*d_theta);
      y = sin((longitude)*d_phi)*sin((latitude+1)*d_theta);
      z = cos((latitude+1)*d_theta);
      glNormal3f(x, y, z);

      //glTexCoord2f(static_cast<float>(longitude)/NR_PHI, static_cast<float>(latitude+1)/NR_THETA);

      glVertex3f(x, y, z);

      glEnd();
  }
}



void worldWidget::faceglobe(){

  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, _image1.Width(), _image1.Height(), 0, GL_RGB, GL_UNSIGNED_BYTE, _image1.imageField());
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

  for(int longitude = 0; longitude < NR_PHI; longitude++)
    for(int latitude = 0; latitude < NR_THETA; latitude++){
      float d_phi   = 2*M_PI/NR_PHI;
      float d_theta = M_PI/NR_THETA;
      glBegin(GL_TRIANGLES);
      double x, y, z;

      x = cos(longitude*d_phi)*sin(latitude*d_theta);
      y = sin(longitude*d_phi)*sin(latitude*d_theta);
      z = cos(latitude*d_theta);
      glNormal3f(x, y, z);

      glTexCoord2f(static_cast<float>(longitude)/NR_PHI, static_cast<float>(latitude)/NR_THETA);

      glVertex3f(x, y, z);
      x = cos((longitude+1)*d_phi)*sin(latitude*d_theta);
      y = sin((longitude+1)*d_phi)*sin(latitude*d_theta);
      z = cos(latitude*d_theta);
      glNormal3f(x, y, z);

      glTexCoord2f(d_phi,0);
      glTexCoord2f(static_cast<float>(longitude+1)/NR_PHI, static_cast<float>(latitude)/NR_THETA);

      glVertex3f(x, y, z);
      x = cos((longitude+1)*d_phi)*sin((latitude+1)*d_theta);
      y = sin((longitude+1)*d_phi)*sin((latitude+1)*d_theta);
      z = cos((latitude+1)*d_theta);
      glNormal3f(x, y, z);


      glTexCoord2f(static_cast<float>(longitude+1)/NR_PHI, static_cast<float>(latitude+1)/NR_THETA);


      glVertex3f(x, y, z);

      x = cos(longitude*d_phi)*sin(latitude*d_theta);
      y = sin(longitude*d_phi)*sin(latitude*d_theta);
      z = cos(latitude*d_theta);
      glNormal3f(x, y, z);


      glTexCoord2f(static_cast<float>(longitude)/NR_PHI, static_cast<float>(latitude)/NR_THETA);


      glVertex3f(x, y, z);
      x = cos((longitude+1)*d_phi)*sin((latitude+1)*d_theta);
      y = sin((longitude+1)*d_phi)*sin((latitude+1)*d_theta);
      z = cos((latitude+1)*d_theta);
      glNormal3f(x, y, z);

      glTexCoord2f(static_cast<float>(longitude+1)/NR_PHI, static_cast<float>(latitude+1)/NR_THETA);


      glVertex3f(x, y, z);
      x = cos((longitude)*d_phi)*sin((latitude+1)*d_theta);
      y = sin((longitude)*d_phi)*sin((latitude+1)*d_theta);
      z = cos((latitude+1)*d_theta);
      glNormal3f(x, y, z);

      glTexCoord2f(static_cast<float>(longitude)/NR_PHI, static_cast<float>(latitude+1)/NR_THETA);

      glVertex3f(x, y, z);

      glEnd();
  }
}


void worldWidget::octagon(){
  //make 3 cylinders
  //2 slightly wider but v squat
  //1 slimmer and longer

  // Here are the normals, correctly calculated for the cube faces below
  GLfloat normals[][3] = { {1., 0. ,0.}, {-1., 0., 0.}, {0., 0., 1.}, {0., 0., -1.}, {1.,0.,-1.}, {1.,0.,1.}, {-1.,0.,-1.}, {-1.,0.,1.}, {0.,1.,0.}, {0.,-1.,0.} };

  //left face
  glNormal3fv(normals[0]);
  glBegin(GL_POLYGON);
    //assuming x,y,z?
    glVertex3f( 1.0, -1.0,  0.5);
    glVertex3f( 1.0, -1.0, -0.5);
    glVertex3f( 1.0,  1.0, -0.5);
    glVertex3f( 1.0,  1.0,  0.5);
    glEnd();

  //right face
  glNormal3fv(normals[1]);
  glBegin(GL_POLYGON);
    glVertex3f( -1.0, -1.0,  0.5);
    glVertex3f( -1.0, -1.0, -0.5);
    glVertex3f( -1.0,  1.0, -0.5);
    glVertex3f( -1.0,  1.0,  0.5);
  glEnd();

  //front face
  glNormal3fv(normals[2]);
  glBegin(GL_POLYGON);
    glVertex3f(-0.5, -1.0, 1.0);
    glVertex3f( 0.5, -1.0, 1.0);
    glVertex3f( 0.5,  1.0, 1.0);
    glVertex3f(-0.5,  1.0, 1.0);
  glEnd();

  //back face
  glNormal3fv(normals[3]);
  glBegin(GL_POLYGON);
    glVertex3f(-0.5, -1.0, -1.0);
    glVertex3f( 0.5, -1.0, -1.0);
    glVertex3f( 0.5,  1.0, -1.0);
    glVertex3f(-0.5,  1.0, -1.0);
  glEnd();


  //make corner faces
  //back right
  glNormal3fv(normals[4]);
  glBegin(GL_POLYGON);
    glVertex3f( 0.5, -1.0, -1.0);
    glVertex3f( 1.0, -1.0, -0.5);
    glVertex3f( 1.0,  1.0, -0.5);
    glVertex3f( 0.5,  1.0, -1.0);
  glEnd();

  //front right
  glNormal3fv(normals[5]);
  glBegin(GL_POLYGON);
    glVertex3f( 0.5, -1.0,  1.0);
    glVertex3f( 1.0, -1.0,  0.5);
    glVertex3f( 1.0,  1.0,  0.5);
    glVertex3f( 0.5,  1.0,  1.0);
  glEnd();

  //back left
  glNormal3fv(normals[6]);
  glBegin(GL_POLYGON);
    glVertex3f( -1.0, -1.0, -0.5);
    glVertex3f( -0.5, -1.0, -1.0);
    glVertex3f( -0.5,  1.0, -1.0);
    glVertex3f( -1.0,  1.0, -0.5);
  glEnd();

  //front left
  glNormal3fv(normals[7]);
  glBegin(GL_POLYGON);
    glVertex3f( -1.0, -1.0,  0.5);
    glVertex3f( -0.5, -1.0,  1.0);
    glVertex3f( -0.5,  1.0,  1.0);
    glVertex3f( -1.0,  1.0,  0.5);
  glEnd();


  //top
  glNormal3fv(normals[8]);
  glBegin(GL_POLYGON);
    glVertex3f( -1.0,  1.0,   0.5);
    glVertex3f( -1.0,  1.0,  -0.5);
    glVertex3f( -0.5,  1.0,  -1.0);
    glVertex3f(  0.5,  1.0,  -1.0);
    glVertex3f(  1.0,  1.0,  -0.5);
    glVertex3f(  1.0,  1.0,   0.5);
    glVertex3f(  0.5,  1.0,   1.0);
    glVertex3f( -0.5,  1.0,   1.0);
  glEnd();

  //don't need bottom never seen by camera.

}//octagon()

//calls 4 octagons of varying size and place
void worldWidget::base(){

  materialStruct* p_front = &brassMaterials;

  glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
  glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
  glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
  glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);

  this->octagon();

  glScalef(1.1,0.2,1.1);
  glTranslatef(0,-4.1,0);

  this->octagon(); //should be the bottom octagon?

  glTranslatef(0,8.2,0);

  this->octagon(); //should be the top octagon?

  glScalef(0.8,0.8,0.8);
  p_front = &greenMaterials;

  glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
  glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
  glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
  glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);
  glTranslatef(0,0.5,0);
  this->octagon(); //should be the top octagon?
  glTranslatef(0,-0.2,0);


}

//used for the house
void worldWidget::cube(){

  // Here are the normals, correctly calculated for the cube faces below
  GLfloat normals[][3] = { {1., 0. ,0.}, {-1., 0., 0.}, {0., 0., 1.}, {0., 0., -1.}};

  glNormal3fv(normals[0]);
  glBegin(GL_POLYGON);
    glVertex3f( 1.0, -1.0,  1.0);
    glVertex3f( 1.0, -1.0, -1.0);
    glVertex3f( 1.0,  1.0, -1.0);
    glVertex3f( 1.0,  1.0,  1.0);
    glEnd();

  glNormal3fv(normals[3]);
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, -1.0);
    glVertex3f( 1.0, -1.0, -1.0);
    glVertex3f( 1.0,  1.0, -1.0);
    glVertex3f(-1.0,  1.0, -1.0);
  glEnd();

  glNormal3fv(normals[2]);
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, 1.0);
    glVertex3f( 1.0, -1.0, 1.0);
    glVertex3f( 1.0,  1.0, 1.0);
    glVertex3f(-1.0,  1.0, 1.0);
  glEnd();

  glNormal3fv(normals[1]);
  glBegin(GL_POLYGON);
    glVertex3f( -1.0, -1.0,  1.0);
    glVertex3f( -1.0, -1.0, -1.0);
    glVertex3f( -1.0,  1.0, -1.0);
    glVertex3f( -1.0,  1.0,  1.0);
  glEnd();

}

void worldWidget::trunk(){
  //get bark image.
  materialStruct* p_front = &brownMaterials;

  glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
  glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
  glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
  glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);

  this->octagon();

}//trunk for tree

void worldWidget::cone(){
  //get leaves iamge?

  GLfloat normals[][3] = { {1., 0. ,0.}, {-1., 0., 0.}, {0., 0., 1.}, {0., 0., -1.}};

  glNormal3fv(normals[0]);
  glBegin(GL_POLYGON);
    glVertex3f( 1.5, -2.0,  1.5);
    glVertex3f( 1.5, -2.0, -1.5);
    glVertex3f( 0.0,  2.0, 0.0);
    glVertex3f( 0.0,  2.0,  0.0);
    glEnd();

  glNormal3fv(normals[3]);
  glBegin(GL_POLYGON);
    glVertex3f(-1.5, -2.0, -1.5);
    glVertex3f( 1.5, -2.0, -1.5);
    glVertex3f( 0.0,  2.0, 0.0);
    glVertex3f( 0.0,  2.0, 0.0);
  glEnd();

  glNormal3fv(normals[2]);
  glBegin(GL_POLYGON);
    glVertex3f(-1.5, -2.0, 1.5);
    glVertex3f( 1.5, -2.0, 1.5);
    glVertex3f( 0.0,  2.0, 0.0);
    glVertex3f( 0.0,  2.0, 0.0);
  glEnd();

  glNormal3fv(normals[1]);
  glBegin(GL_POLYGON);
    glVertex3f(-1.5, -2.0,  1.5);
    glVertex3f(-1.5, -2.0, -1.5);
    glVertex3f( 0.0,  2.0, 0.0);
    glVertex3f( 0.0,  2.0,  0.0);
  glEnd();

}//cone for tree

void worldWidget::dude(){
  materialStruct* b_front = &blueMaterials;
  materialStruct* r_front = &redMaterials;
  materialStruct* be_front = &beigeMaterials;
  materialStruct* br_front = &brownMaterials;
  materialStruct* w_front = &whiteShinyMaterials;


  glMaterialfv(GL_FRONT, GL_AMBIENT,    b_front->ambient);
  glMaterialfv(GL_FRONT, GL_DIFFUSE,    b_front->diffuse);
  glMaterialfv(GL_FRONT, GL_SPECULAR,   b_front->specular);
  glMaterialf(GL_FRONT, GL_SHININESS,   b_front->shininess);

  // give boi legs,torso,head, shoulders,arms
  // head needs moi.ppm
  // arms will chop in a cycle and then be returned to the start possition.
  //make upper arms rotate as standard,
  //make forearms do auto rotation THEN bend in to grab the axe.

  //legs below
  glScalef(.25 ,.25,.25);
  glRotatef(90,1,0,0);
  //make legses
  glTranslatef(0,.5,2);

  gluCylinder(plegl,.3,.3,2,5,5);
  glTranslatef(0,-1,0);
  gluCylinder(plegl,.3,.3,2,5,5);
  //legs above


  glMaterialfv(GL_FRONT, GL_AMBIENT,    r_front->ambient);
  glMaterialfv(GL_FRONT, GL_DIFFUSE,    r_front->diffuse);
  glMaterialfv(GL_FRONT, GL_SPECULAR,   r_front->specular);
  glMaterialf(GL_FRONT, GL_SHININESS,   r_front->shininess);

  glTranslatef(0,.5,-2);
  glScalef(.5,1,1);

  gluCylinder(ptorso,1.0,.7,2,5,5);


  glScalef(2,1,1);
  glMaterialfv(GL_FRONT, GL_AMBIENT,    be_front->ambient);
  glMaterialfv(GL_FRONT, GL_DIFFUSE,    be_front->diffuse);
  glMaterialfv(GL_FRONT, GL_SPECULAR,  be_front->specular);
  glMaterialf(GL_FRONT, GL_SHININESS,   be_front->shininess);
  glTranslatef(0,0,-1);
  this->faceglobe();

  glScalef(2,2,2);
  glRotatef(-90,1,0,0);

  glMaterialfv(GL_FRONT, GL_AMBIENT,    r_front->ambient);
  glMaterialfv(GL_FRONT, GL_DIFFUSE,    r_front->diffuse);
  glMaterialfv(GL_FRONT, GL_SPECULAR,   r_front->specular);
  glMaterialf(GL_FRONT, GL_SHININESS,   r_front->shininess);
  glTranslatef(0,-.5,-.5);

  gluCylinder(pshoulders,.25,.25,1,5,5);

  //upper arms
  glRotatef(90,1,0,0);
  glRotatef(armangle,0,1,0);
  gluCylinder(pupperl,.125,.125,.7,5,5);
  glTranslatef(0,1,0);
  gluCylinder(pupperr,.125,.125,.7,5,5);

  glMaterialfv(GL_FRONT, GL_AMBIENT,    be_front->ambient);
  glMaterialfv(GL_FRONT, GL_DIFFUSE,    be_front->diffuse);
  glMaterialfv(GL_FRONT, GL_SPECULAR,   be_front->specular);
  glMaterialf(GL_FRONT, GL_SHININESS,   be_front->shininess);


  //lower arms
  glTranslatef(.0,.0,.7);
  glRotatef(armangle,0,1,0);
  glRotatef(30,1,0,0);
  gluCylinder(plowerr,.125,.125,.7,5,5);
  glRotatef(-30,1,0,0);
  glTranslatef(0,-1,0);
  glRotatef(-30,1,0,0);
  gluCylinder(plowerl,.125,.125,.7,5,5);


  //hands
  glRotatef(30,1,0,0);
  glTranslatef(0,.5,.7);
  gluSphere(phands,.2,5,5);

  glMaterialfv(GL_FRONT, GL_AMBIENT,    br_front->ambient);
  glMaterialfv(GL_FRONT, GL_DIFFUSE,    br_front->diffuse);
  glMaterialfv(GL_FRONT, GL_SPECULAR,   br_front->specular);
  glMaterialf(GL_FRONT, GL_SHININESS,   br_front->shininess);

  //axe
  glTranslatef(.3,0,0);

  glRotatef(-90,0,1,0);
  gluCylinder(paxe,.125,.125,1.5,5,5);
  glTranslatef(0,0,1.2);
  glScalef(0.1,0.1,0.1);
  glRotatef(-90,0,0,1);
  this->house();

}


void worldWidget::house(){
  glScalef(2,2,2);
  materialStruct* p_front = &brownMaterials;

  glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
  glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
  glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
  glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);

  this->cube();
  glTranslatef(0,1.7,0);
  glScalef(1.2,1.,1.2);
  p_front = &redMaterials;

  glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
  glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
  glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
  glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);

  this->roof();
  glScalef(0.833333,1,0.833333);
  glTranslatef(0,-1.7,0);
}//house

void worldWidget::tree(){
  glScalef(1,2,1);

  this->trunk();
  glTranslatef(0,2,0);

  materialStruct* p_front = &greenMaterials;

  glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
  glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
  glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
  glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);

  this->cone();
  glTranslatef(0,-2,0);
  glScalef(1,0.5,1);


}//tree


void worldWidget::sky(){
  //make a glowing orb

  //wait for rotation to be equal to 45 before activating other emission shader
  glTranslatef(0,-1,0);
  glRotatef(skyrotation,0,0,1);

  materialStruct* p_front;

  glTranslatef(5,0,0);
  p_front = &sunMaterials;

  glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
  glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
  glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);
  glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
  if (skyrotation>0){
    glMaterialfv(GL_FRONT, GL_EMISSION,   p_front->ambient);
  }
  this->globe();


  p_front = &blueMaterials;

  glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
  glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
  glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);
  glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);

  //if above the horizon make emit
  if (skyrotation<0){
    glMaterialfv(GL_FRONT, GL_EMISSION,   p_front->ambient);
  }
  glTranslatef(-10,0,0);
  this->globe();
  glTranslatef(5,0,0);

  glRotatef(-skyrotation,0,0,1);
  glTranslatef(0,1,0);
}//sky - this will be the sun/moon


void worldWidget::roof(){
  GLfloat normals[][3] = { {1., 0. ,0.}, {-1., 0., 0.}, {0., 0., 1.}, {0., 0., -1.}};

  glNormal3fv(normals[0]);
  glBegin(GL_POLYGON);
    glVertex3f( 1.0, -1.0,  1.0);
    glVertex3f( 1.0, -1.0, -1.0);
    glVertex3f( 0.0,  1.0, -1.0);
    glVertex3f( 0.0,  1.0,  1.0);
    glEnd();

  glNormal3fv(normals[3]);
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, -1.0);
    glVertex3f( 1.0, -1.0, -1.0);
    glVertex3f( 0.0,  1.0, -1.0);
    glVertex3f( 0.0,  1.0, -1.0);
  glEnd();

  glNormal3fv(normals[2]);
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, 1.0);
    glVertex3f( 1.0, -1.0, 1.0);
    glVertex3f( 0.0,  1.0, 1.0);
    glVertex3f( 0.0,  1.0, 1.0);
  glEnd();

  glNormal3fv(normals[1]);
  glBegin(GL_POLYGON);
    glVertex3f( -1.0, -1.0,  1.0);
    glVertex3f( -1.0, -1.0, -1.0);
    glVertex3f( 0.0,  1.0, -1.0);
    glVertex3f( 0.0,  1.0,  1.0);
  glEnd();
} // roof of the house



void worldWidget::worldmap(){

  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, _image2.Width(), _image2.Height(), 0, GL_RGB, GL_UNSIGNED_BYTE, _image2.imageField());
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  materialStruct* p_front = &whiteShinyMaterials;

  glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
  glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
  glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
  glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);

  // Here are the normals, correctly calculated for the cube faces below
  GLfloat normals[][3] = { {1., 0. ,0.}, {-1., 0., 0.}, {0., 0., 1.}, {0., 0., -1.}};

  glNormal3fv(normals[0]);
  glBegin(GL_POLYGON);
  glTexCoord2f(0.0, 0.0);
  glVertex3f(-1.0, -1.0, 1.0);
  glTexCoord2f(1.0, 0.0);
  glVertex3f( 1.0, -1.0, 1.0);
  glTexCoord2f(1.0, 1.0);
  glVertex3f( 1.0,  1.0, 1.0);
  glTexCoord2f(0.0, 1.0);
  glVertex3f(-1.0,  1.0, 1.0);
    glEnd();

    p_front = &brownMaterials;

    glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
    glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);

  glNormal3fv(normals[3]);
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, -1.0);
    glVertex3f( 1.0, -1.0, -1.0);
    glVertex3f( 1.0,  1.0, -1.0);
    glVertex3f(-1.0,  1.0, -1.0);
  glEnd();

  glNormal3fv(normals[2]);
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, 1.0);
    glVertex3f( 1.0, -1.0, 1.0);
    glVertex3f( 1.0,  1.0, 1.0);
    glVertex3f(-1.0,  1.0, 1.0);
  glEnd();

  glNormal3fv(normals[1]);
  glBegin(GL_POLYGON);
    glVertex3f( -1.0, -1.0,  1.0);
    glVertex3f( -1.0, -1.0, -1.0);
    glVertex3f( -1.0,  1.0, -1.0);
    glVertex3f( -1.0,  1.0,  1.0);
  glEnd();


}

void worldWidget::map(){

  //two pillars and a box in the middle to put the map texture on.
  glScalef(.25,1,.25);
  glTranslatef(0,0,18);

  this->trunk();

  glTranslatef(0,0,-9);
  glScalef(1,1,8);
  //make a custom plane with the world map on it.
  glRotatef(90,0,1,0);
  this->worldmap();
  glRotatef(-90,0,1,0);

  glScalef(1,1,.125);
  glTranslatef(0,0,-9);

  this->trunk();
  glScalef(4,1,4);
}

void worldWidget::bird(){
  //head /w beak
  //body behind
  //wings either side of body.
  materialStruct* p_front = &sunMaterials;

  glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
  glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
  glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
  glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);
  //do right side of wings then step backwards like a stack
  //do left as mirror of the right.
  glScalef(0.25,0.25,0.25);
  this->cone();
  glScalef(4,4,4);



  glTranslatef(0,-1.5,0);

  p_front = &mattBlack;

  glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
  glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
  glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
  glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);

  this->globe();

  glTranslatef(0,-2.5,0);
  glScalef(1,2,1);
  this->globe();

  //put in the wings!

  glScalef(1,.5,.5);

  glTranslatef(2,0,0);

  glRotatef(flapAngle,0,1,0);

    glScalef(1,1,.5);
    this->cube();
    glScalef(1,1,2);

  glTranslatef(2,0,0);

  glRotatef(flapAngle,0,1,0);

    glScalef(1,1,.5);
    this->cube();
    glScalef(1,1,2);

  glRotatef(-flapAngle,0,1,0);

  glTranslatef(-2,0,0);
  glRotatef(-flapAngle,0,1,0);



  glTranslatef(-4,0,0);

  glRotatef(-flapAngle,0,1,0);

    glScalef(1,1,.5);
    this->cube();
    glScalef(1,1,2);

  glTranslatef(-2,0,0);

  glRotatef(-flapAngle,0,1,0);

    glScalef(1,1,.5);
    this->cube();
    glScalef(1,1,2);

  glRotatef(flapAngle,0,1,0);

  glTranslatef(2,0,0);
  glRotatef(flapAngle,0,1,0);
  glTranslatef(2,0,0);

  glScalef(1,2,2);


  //glTranslatef(-1,0,0);
  /////////////////////////
  glScalef(1,.5,1);
  glTranslatef(0,4,0);

}




//inside the globe
void worldWidget::internal(){

  glTranslatef(0.,0.,-.5);

  glRotatef(90,1.,0.,0.);
  glScalef(0.04,0.04,0.04);
  //moving between all these trees.
  glTranslatef(10,0,10);
  this->tree();
  glTranslatef(-20,0,-20);


  this->tree();
  //this->tree();
  glTranslatef(10,-2,10);
  this->trunk();
  glTranslatef(0,2,0);
  glScalef(2,2,2);
  this->sky();
  glScalef(0.5,0.5,0.5);


  //make a map stand
  glTranslatef(-5,0,10);
  glRotatef(45,0,1,0);
  this->map();
  glRotatef(-45,0,1,0);
  glTranslatef(5,0,-10);


  glTranslatef(0,0,-10);
  glRotatef(90,0,1,0);
  glScalef(1,1,2);
  this->house();

  glScalef(1,1,0.5);
  glTranslatef(-4,0,0);

  this->dude();


  glTranslatef(0,6,5);
  glScalef(13.7,13.7,13.7);
  glRotatef(-90,1,0,0);
}//internal





/////////////////////////////////////////
//slots
///////////////////////////////////////

void worldWidget::update_timer()
  {

    birdrotation++;

    //increment angle here
    if (ROTATE){
      rotation -=.5;
    }
    if (skyrotation<55 && DAY){
      skyrotation++;
    } else if (skyrotation>-55 && !DAY) {
      skyrotation--;
    }

    if(FLAP&&flapAngle<45){
      flapAngle++;
    } else if(!FLAP && flapAngle>-45){
      flapAngle--;
    }

    if(flapAngle>=45){
      FLAP=false;
    } else if (flapAngle<=-45){
      FLAP=true;
    }

    if(ANIMATE && armangle>-80){
      armangle-=5;
    } else if (!ANIMATE && armangle<-20){
      armangle+=10;
    }

    if (armangle<=-80){
      ANIMATE=false;
    }



    glFlush();
    this->update();
  } //my timer slot



void worldWidget::updateZoom(int i){
  zoom = (double)i*.05;
  this->repaint();
}

void worldWidget::toggleDayNight(){
  if(DAY){
    DAY=false;
  }
  else if(!DAY){
    DAY=true;
  }
}

void worldWidget::toggleRotation(){
  if(ROTATE){
    ROTATE=false;
  }
  else if(!ROTATE){
    ROTATE=true;
  }
}

void worldWidget::animate(){
  ANIMATE=true;
}

/////////////////////////////////////////
//slots
///////////////////////////////////////

void worldWidget::MyTimer()
  {
    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()),this,SLOT(update_timer()));
    timer->start();
  }

// called every time the widget needs painting
void worldWidget::paintGL()
	{
  glPushMatrix();
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	GLfloat light_pos[] = {60,60,60, .0};
	glLightfv(GL_LIGHT0, GL_POSITION, light_pos);
  glLightf (GL_LIGHT0, GL_SPOT_CUTOFF,200.);




  // clear the widget
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// You must set the matrix mode to model view directly before enabling the depth test

	glEnable(GL_DEPTH_TEST); // comment out depth test to observe the result
  glPopMatrix();

  glPushMatrix();

  glTranslatef(0,-zoom*zoom,0);
  glRotatef(rotation,0,1,0);
  glShadeModel(GL_FLAT);
  glScalef(3.,3.,3.);
  glScalef(zoom, zoom,  zoom);

  this->base();

  glScalef(1.2,1.2,1.2);
  glRotatef(270,1,0,0);

  glShadeModel(GL_SMOOTH);
  glScalef(1,1,5);
  glTranslatef(0,0,.75);

  glScalef(1.15,1.15,1.15);

  glScalef(.01,.01,.01);
  glRotatef(birdrotation,0,0,1);
  glTranslatef(25,0,5);
  this->bird();
  glTranslatef(-25,0,-5);
  glRotatef(-birdrotation,0,0,1);
  glScalef(100,100,100);




  glTranslatef(0,0,.1);
  this->internal();



  glPopMatrix();

  //make the globe be glass
  glPushMatrix();
  materialStruct* p_front = &glassMaterials;

  glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
  glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
  glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
  glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);
  glScalef(3.5,3.5,3.5);
  if (zoom<.5){
    glTranslatef(0,1.2*zoom,0);

    glScalef(zoom,zoom,zoom);
    this->globe();

  }

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
  glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

  gluLookAt(2,3,2, -1,0,-1, 0.0,1.0,0.0);

	// flush to screen
	glFlush();

	} // paintGL()
