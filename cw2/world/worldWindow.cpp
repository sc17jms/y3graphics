#include "worldWindow.h"

// constructor / destructor
worldWindow::worldWindow(QWidget *parent)
	: QWidget(parent)
	{ // constructor

	// create menu bar
	menuBar = new QMenuBar(this);

	// create file menu
	fileMenu = menuBar->addMenu("&File");

	// create the action
	actionQuit = new QAction("&Quit", this);

	// leave signals & slots to the controller

	// add the item to the menu
	fileMenu->addAction(actionQuit);

	// create the window layout
	windowLayout = new QBoxLayout(QBoxLayout::TopToBottom, this);
	// create main widget
	renderWidget = new worldWidget(this);
	windowLayout->addWidget(renderWidget);

	// create slider
	scaleSlider = new QSlider(Qt::Horizontal);
	scaleSlider->setMinimum(5);
	scaleSlider->setMaximum(15);
	connect(scaleSlider, SIGNAL(valueChanged(int)), renderWidget, SLOT(updateZoom(int)));

	windowLayout->addWidget(scaleSlider);

	//3 buttons
	dayButton =  new QPushButton(this);
	dayButton->setText("Toggle Day/Night");
	connect(dayButton, SIGNAL (released()), renderWidget, SLOT(toggleDayNight()));

	rotateButton =  new QPushButton(this);
	rotateButton->setText("Toggle Rotation");
	connect(rotateButton, SIGNAL (released()), renderWidget, SLOT(toggleRotation()));

	animateButton =  new QPushButton(this);
	animateButton->setText("Chop!");
	connect(animateButton, SIGNAL (released()), renderWidget, SLOT(animate()));

	windowLayout->addWidget(dayButton);
	windowLayout->addWidget(rotateButton);
	windowLayout->addWidget(animateButton);
	} // constructor

worldWindow::~worldWindow()
	{ // destructor
	delete scaleSlider;
	delete renderWidget;
	delete windowLayout;
	delete actionQuit;
	delete fileMenu;
	delete menuBar;
	} // destructor

// resets all the interface elements
void worldWindow::ResetInterface()
	{ // ResetInterface()
	scaleSlider->setMinimum(5);
	scaleSlider->setMaximum(20);

	//don't use the slider for now
	//scaleSlider->setValue(renderWidget->zoom);

	// now force refresh
	renderWidget->update();
	update();
	} // ResetInterface()
