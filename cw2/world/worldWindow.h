#ifndef __GL_POLYGON_WINDOW_H__
#define __GL_POLYGON_WINDOW_H__ 1

#include <QGLWidget>
#include <QMenuBar>
#include <QSlider>
#include <QPushButton>
#include <QBoxLayout>
#include "worldWidget.h"

class worldWindow: public QWidget
	{
	public:


	// constructor / destructor
	worldWindow(QWidget *parent);
	~worldWindow();

	// visual hierarchy
	// menu bar
	QMenuBar *menuBar;
		// file menu
		QMenu *fileMenu;
			// quit action
			QAction *actionQuit;

	// window layout
	QBoxLayout *windowLayout;

	// beneath that, the main widget
	worldWidget *renderWidget;
	// and a slider for the scale
	QSlider *scaleSlider;
	QPushButton *dayButton;
	QPushButton *rotateButton;
	QPushButton *animateButton;

	// resets all the interface elements
	void ResetInterface();
	};

#endif
